#!/bin/bash

# Optionally takes a folder name, if none has been
# specified, use current time and date
TRACE_NAME=""
if [ "$#" -ne 1 ]; then
	TRACE_NAME=$(date +%Y-%m-%dT%H:%M:%S%z)
else
	TRACE_NAME=$1
fi

#### Build code ####
cd environment
make clean
make
cd ../controller
make clean
make
cd ..

#### Gather traces ####
# Ignore output, as error is returned if already running
lttng-sessiond --daemonize || true
lttng create toy_example_trace -o ./traces/$TRACE_NAME
# Select tracepoints
lttng enable-event -u toy_tracepoint:sensor
lttng enable-event -u toy_tracepoint:barrier
lttng enable-event -u toy_tracepoint:signal
lttng enable-event -u toy_tracepoint:event
echo "Starting tracing session"
lttng start
./controller/example
lttng destroy
echo "Successfully created traces in ./traces/$TRACE_NAME"