#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER toy_tracepoint

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./tracepoints.h"

#if !defined(_TRACEPOINTS_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TRACEPOINTS_H

#include <lttng/tracepoint.h>

TRACEPOINT_EVENT(
	toy_tracepoint,
	sensor,
	TP_ARGS(
		char*, sensor_id_arg, /* 'a', 'b' or 'c' */
		int, activated_arg /* boolean, is activated */
	),
	TP_FIELDS(
		ctf_string(sensor, sensor_id_arg)
		ctf_integer(char, has_train, activated_arg)
	)
)

TRACEPOINT_EVENT(
	toy_tracepoint,
	barrier,
	TP_ARGS(
		char*, barrier_id_arg, /* "north" or "south" */
		int, start_n_finished_arg, /* Start, not finished*/
		int, is_up_or_moving_up_arg /* up, moving up, not down, not moving down */
	),
	TP_FIELDS(
		ctf_string(barrier_id, barrier_id_arg)	
		ctf_integer(char, is_moving, start_n_finished_arg)
		ctf_integer(char, is_up, is_up_or_moving_up_arg)
	)
)

TRACEPOINT_EVENT(
	toy_tracepoint,
	signal,
	TP_ARGS(
		int, is_stop_arg /* stop, not go */
	),
	TP_FIELDS(
		ctf_integer(char, is_stop, is_stop_arg)
	)
)

TRACEPOINT_EVENT(
	toy_tracepoint,
	event,
	TP_ARGS(
		char*, event_name_arg, /* String to identify event */
		int, start_n_end_arg /* Start, not end */
	),
	TP_FIELDS(
		ctf_string(event_name, event_name_arg)
		ctf_integer(char, is_start, start_n_end_arg)
	)
)


#endif /* _TRACEPOINTS_H */

#include <lttng/tracepoint-event.h>
