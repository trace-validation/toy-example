#include <stdlib.h>

#include "actions.h"


typedef struct {
    int type;
    int instance;
    int action;
} action, *p_action;

/** List of the actions to be executed */
p_action p_action_list = NULL;
int action_count = 0;

int schedule_action(enum types type, int instance, int action)
{
    if (p_action_list == NULL) {
        // Only three actions are possible per iteration, so allocate for that
        p_action_list = malloc(3*sizeof(action));
    }
    
    if (action_count == 3) return 1;
    
    p_action_list[action_count].type = type;
    p_action_list[action_count].instance = instance;
    p_action_list[action_count].action = action;
    action_count++;
    
    return 0;
}

void handle_actions()
{
    if (p_action_list == NULL) return;
    
    while(action_count > 0) {
        // Process this action
        action this_action = p_action_list[--action_count];
        
        if (this_action.type == barrier) {
            move_barrier(this_action.instance, this_action.action);
        } else if (this_action.type == rail_signal) {
            set_signal(this_action.action);
        } else {
            // Error
        }
    }
}
