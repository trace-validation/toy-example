#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "../environment/interface.h"
#include "actions.h"
#include "tracepoints.h"

#ifndef N_OF_CYCLES
#define N_OF_CYCLES (10000)
#endif

/** Execute one iteration of control loop */
void run();
/** Get current value of all sensors */
void retrieve_status();
/** Determine which actions should be undertaken in the current state */
void determine_actions();
/** Save copy of current state to compare to next iteration */
void set_previous_status();

int main()
{
    printf("Starting controller...\n");
#ifdef TRACING
	printf("Generating traces...\n");
#endif
    int count = 0;
    
    // Start controller loop
    while(1) {		
        if (count++ == N_OF_CYCLES) return 0;
#ifdef TRACING
		tracepoint(toy_tracepoint, event, "loop", 1);
#endif

		// Execute controller
        run();
#ifdef DRAW_TRAIN
		// Draw current state
        if (count % 100 == 0) print_train();
#endif
        
#ifdef TRACING
		tracepoint(toy_tracepoint, event, "loop", 0);
#endif

        /*
         Execute run() every 5ms.
         Not exact 5ms intervals, but the exact interval doesn't matter anyway.
         */
        unsigned int usecs = 5000;
        usleep(usecs);
    }
    
    return 0;
}

void run()
{
    /* Run phases of controller loop once */
#ifdef TRACING
	tracepoint(toy_tracepoint, event, "retrieve_status", 1);
#endif
    retrieve_status();
#ifdef TRACING
	tracepoint(toy_tracepoint, event, "retrieve_status", 0);
	tracepoint(toy_tracepoint, event, "determine_actions", 1);
#endif
    determine_actions();
#ifdef TRACING
	tracepoint(toy_tracepoint, event, "determine_actions", 0);
	tracepoint(toy_tracepoint, event, "handle_actions", 1);
#endif
    handle_actions();
#ifdef TRACING
	tracepoint(toy_tracepoint, event, "handle_actions", 0);
#endif
    set_previous_status();
}

/* System status variables */
enum sensor_state       sensors[3];
enum barrier_position   barriers[2];
enum signal_state       current_signal = go;

enum sensor_state       prev_sensors[3];
enum barrier_position   prev_barriers[2];

void retrieve_status()
{
    int i;
    
    // Sensors
    for (i=0; i<3; ++i) {
        sensors[i] = get_sensor_state(i);
#ifdef TRACING
		char sensor_label[2] = {'a'+i, '\0'};
		tracepoint(toy_tracepoint, sensor, sensor_label, sensors[i]);
#endif
    }
    
    // Barriers
    for (i=0; i<2; ++i) {
        barriers[i] = get_barrier_position(i);
    }
    
    if (barriers[north] != prev_barriers[north] &&
            prev_barriers[north] == halfway) {
        printf("Barrier NORTH has moved %s\n",
                barriers[north] == up ? "up" : "down");
#ifdef TRACING
		tracepoint(toy_tracepoint, barrier, "north", 0, barriers[north]==up);
#endif
    }
    if (barriers[south] != prev_barriers[south] &&
            prev_barriers[south] == halfway) {
        printf("Barrier SOUTH has moved %s\n",
                barriers[south] == up ? "up" : "down");
#ifdef TRACING
		tracepoint(toy_tracepoint, barrier, "south", 0, barriers[south]==up);
#endif
    }
}

void determine_actions()
{
    // If sensor A has a train for the first time, start lowering barriers
    if (sensors[sensor_a] == train &&
            prev_sensors[sensor_a] == no_train) {
        schedule_action(barrier, north, down);
        schedule_action(barrier, south, down);
#ifdef TRACING
		tracepoint(toy_tracepoint, barrier, "north", 1, 0);
		tracepoint(toy_tracepoint, barrier, "south", 1, 0);
#endif
    }
    
    // If sensor B has a train for the first time, and the barriers are not
    // down yet, set signal to stop
    if (sensors[sensor_b] == train &&
            prev_sensors[sensor_b] == no_train &&
            (barriers[north] != down || barriers[south] != down)
            ) {
        schedule_action(rail_signal, 0, stop);
#ifdef TRACING
		tracepoint(toy_tracepoint, signal, 1);
#endif
        current_signal = stop;
    }
    
    // If signal is stop, and the barriers are lowered for the first time
    if (current_signal == stop &&
            (prev_barriers[north] != down || prev_barriers[south] != down) &&
            barriers[north] == down &&
            barriers[south] == down) {
        schedule_action(rail_signal, 0, go);
#ifdef TRACING
		tracepoint(toy_tracepoint, signal, 0);
#endif
        current_signal = go;
    }
    
    // If sensor C had a train, but now not anymore for the first time,
    // raise barriers
    if (prev_sensors[sensor_c] == train &&
            sensors[sensor_c] == no_train) {
        schedule_action(barrier, north, up);
        schedule_action(barrier, south, up);
#ifdef TRACING
		tracepoint(toy_tracepoint, barrier, "north", 1, 1);
		tracepoint(toy_tracepoint, barrier, "south", 1, 1);
#endif
    }
}

void set_previous_status()
{
    memcpy(prev_sensors,    sensors,    3*sizeof(enum sensor_state));
    memcpy(prev_barriers,   barriers,   2*sizeof(enum barrier_position));
}
