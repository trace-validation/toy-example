#ifndef ACTIONS_H_
#define ACTIONS_H_

#include "../environment/interface.h"

enum types {
    barrier = 0,
    rail_signal
};

/**
 Schedule an action 
 */
int schedule_action(enum types type, int instance, int action);

/**  */
void handle_actions();


#endif /* ACTIONS_H_ */
