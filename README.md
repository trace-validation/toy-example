# Toy Example

This project contains a railway-related example to test trace validation.

This system models a simple level crossing. Trains can pass in one direction (from left to right).
Before the train reaches the crossing, it passes two sensors that detect if a train is there.
On each side of the track there is a barrier to stop traffic when necessary. A barrier can be up or down. Raising and lowering the barrier takes some time. After the crossing, the train passes another sensor.
There is a stop/go signal right before the crossing that can stop the train if necessary. By default this will be 'go'.
This behaviour will be handled by the controller.

Schematically, this looks as follows:

```
                                                    | north |
                                          signal    |barrier|
-|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|-------|--|--|--|--|-
 |  |  |  |  | ---->  |  |  |  |  |  |  |  |  |  |  |       |  |  |  |  |
-|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|-------|--|--|--|--|-
       ^                              ^             | south |        ^
    sensor a                       sensor b         |barrier|     sensor c
```

This system lives in a simplified world. A train has a speed, but has infinite acceleration/deceleration. No hardware components ever break and there exists no human failure. Trains cannot exist such that (parts of) more than one train is between the two outermost sensors. The track is free of any obstacles, just like the crossing when the barriers are all the way down.

## Architecture
The environment described by the previous section is modelled in the folder `environment/`. This enviroment is controlled by the main application, the controller. The controller code is available in `controller/`.

## Usage

### Requirements
* GCC (was developed for version `8.3.0`)
* GNU Make
* LTTng, including LTTng-UST (not for the `no-tracing` branch)

### Building
There is an automated script to build and gather traces in one command. This script is called `tracing.sh`. Note that the program will run 10000 cycles with the current settings (see `controller/controller.c`). The script takes an optional parameter to name the folder to store the trace.

Alternatively, use the Makefiles in both folders. First, build `libenvironment.a`. After that, build the controller. This will produce the `example` executable.

### Settings
The environment has a set of parameters to alter its behaviour. Those are available in `environment/config.h`. Its structure also allows the definitions to be made in the build command.
