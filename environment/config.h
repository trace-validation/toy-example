#ifndef CONFIG_H_
#define CONFIG_H_

/** Train settings */
#ifndef TRAIN_MAX_SPEED
// Meter/second
#define TRAIN_MAX_SPEED (50)
#endif

#ifndef TRAIN_MIN_SPEED
// Meter/second
#define TRAIN_MIN_SPEED (1)
#endif

#ifndef TRAIN_MAX_LENGTH
// 10*meter
#define TRAIN_MAX_LENGTH (250)
#endif

#ifndef TRAIN_MIN_LENGTH
// 10*meter
#define TRAIN_MIN_LENGTH (1)
#endif

#ifndef TRAIN_MAX_INTERVAL
// Seconds
#define TRAIN_MAX_INTERVAL (180)
#endif

/** Barrier settings */
#ifndef BARRIER_MAX_MOVE_TIME
// Seconds
#define BARRIER_MAX_MOVE_TIME (5)
#endif

#endif /* CONFIG_H_ */