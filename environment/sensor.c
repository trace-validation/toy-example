#include "sensor.h"
#include "interface.h"

#include "train.h"

enum sensor_state get_sensor_state(enum sensor_location location)
{
    double head = get_front_location();
    double tail = get_back_location();
    double sensor_location;
    
    switch (location) {
        case sensor_a:
            sensor_location = DIST_A;
            break;
        case sensor_b:
            sensor_location = DIST_A+DIST_B;
            break;
        case sensor_c:
            sensor_location = DIST_TOTAL;
            break;
        default:
            // Error
            return no_train;
    }
    
    if (head > sensor_location && tail < sensor_location) return train;
    return no_train;
}
