#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "barrier.h"
#include "interface.h"
#include "config.h"

float get_random_move_time();
double time_diff(struct timespec* a, struct timespec* b);

// Northern barrier
enum barrier_position n_position = up;
struct timespec n_start_move;
float n_move_duration = 0.0;
enum barrier_position n_moving_direction = up;

// Southern barrier
enum barrier_position s_position = up;
struct timespec s_start_move;
float s_move_duration = 0.0;
enum barrier_position s_moving_direction = up;

void move_barrier(enum barrier_location location, enum barrier_position position)
{
    if (position == halfway) return;
    
    if (location == north) {
        clock_gettime(CLOCK_MONOTONIC, &n_start_move);
        n_move_duration     = get_random_move_time();
        n_position          = halfway;
        n_moving_direction  = position;
        printf("Move barrier NORTH %s in %fs\n", position == up ? "up" : "down", n_move_duration);
        return;
    } else {
        clock_gettime(CLOCK_MONOTONIC, &s_start_move);
        s_move_duration     = get_random_move_time();
        s_position          = halfway;
        s_moving_direction  = position;
        printf("Move barrier SOUTH %s in %fs\n", position == up ? "up" : "down", s_move_duration);
        return;
    }
}

enum barrier_position get_barrier_position(enum barrier_location location)
{
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    
    if (location == north) {
        if (n_position != halfway) return n_position;
        if (time_diff(&now, &n_start_move) > n_move_duration) {
            // Raising/lowering has finished
            n_position = n_moving_direction;
        }
        return n_position;
    } else {
        if (s_position != halfway) return s_position;
        if (time_diff(&now, &s_start_move) > s_move_duration) {
            // Raising/lowering has finished
            s_position = s_moving_direction;
        }
        return s_position;
    }
}

float get_random_move_time()
{
    float time = rand() % (BARRIER_MAX_MOVE_TIME-1);
    time += (rand() % 100) / 100.0; // Add 1/100th resolution, max 1 second
    
    return time;
}

double time_diff(struct timespec* a, struct timespec* b) {
    int sec = a->tv_sec - b->tv_sec;
    int ms = a->tv_nsec/1e6 - b->tv_nsec/1e6; // To ms
    
    return sec + ms/1000;
}
