#ifndef SIGNAL_H_
#define SIGNAL_H_

/*
 The signal is positioned as followed:
                           |     |
> -|--|--|--|--|--|--|--|--|     |--|-
> -|--|--|--|--|--|--|--|--|     |--|-
                           |     |
   ^                 ^  ^
   sensor_a   sensor_b  signal
 
                     |  |
                    DIST_S
 */

/**
 Ensure that DIST_S < DIST_C - DIST_B &&
 DIST_S-DIST_B > speed/sample time
 */
#define DIST_S 10.0+DIST_A+DIST+B // meter

enum signal_state {
    stop = 0,
    go
};

/**
 Returns the current setting of the signal
 */
enum signal_state get_signal();

#endif /* SIGNAL_H_ */
