#ifndef TRAIN_H_
#define TRAIN_H_

/**
 Get the front position of the train
 */
double get_front_location();

/**
 Get the rear position of the train
 */
double get_back_location();

/**
 'Pause' the train, such that it doesn't move
 */
void pause_train();

/**
 'Continue' the train, such that it moves again
 */
void start_train();

#endif /* TRAIN_H_ */
