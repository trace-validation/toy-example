#ifndef BARRIER_H_
#define BARRIER_H_

/*
 The barriers are placed as follows:
                |NORTH|
                |     |
 > -|--|--|--|--|     |--|-
 > -|--|--|--|--|     |--|-
                |     |
                |SOUTH|
 */

enum barrier_position {
    up = 0,
    halfway,
    down
};

enum barrier_location {
    north = 0,
    south
};

#endif /* BARRIER_H_ */
