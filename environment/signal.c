#include <stdio.h>

#include "signal.h"
#include "interface.h"

#include "train.h"

enum signal_state current_state;

void set_signal(enum signal_state state)
{
    current_state = state;
    
    if (state == stop) {
        pause_train();
    } else {
        start_train();
    }
        
    printf("Signal set to %s\n", state == go ? "go" : "stop");
}

enum signal_state get_signal()
{
    return current_state;
}
