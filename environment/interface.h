#ifndef INTERFACE_H_
#define INTERFACE_H_

/**
 Interface between environment and controller
 */

#include "sensor.h"
#include "barrier.h"
#include "signal.h"

/**
    Get the current state of the selected sensor
    @param location indication of which sensor should be queried
    @return train or no_train depending on whether a train is located at the
            position of the sensor
 */
enum sensor_state get_sensor_state(enum sensor_location location);

/**
 Set railside signal to go or stop
 */
void set_signal(enum signal_state state);

/**
    Move the selected barrier to one of two positions: up or down.
    @param location indication whether to move the north or south barrier
    @param position whether to start moving up or down. Halfway is an invalid position
 */
void move_barrier(enum barrier_location location,
                  enum barrier_position position);

/**
    Get the current position of the selected barrier
    @param location indication whether to obtain positoin of north or south barrier
    @return current position of barrier. May be up, down or halfway.
 */
enum barrier_position get_barrier_position(enum barrier_location location);

/**
 Visualisation, display the location of the train
 */
void print_train();

#endif /* INTERFACE_H_ */
