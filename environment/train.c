#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "train.h"
#include "interface.h"
#include "config.h"

#include "sensor.h"

double speed = 0.0; // m/s
double length = -(DIST_TOTAL)-1.0; // meter, init s.t. train is generated

struct timespec appear_time;

struct timespec pause_time;
double paused_front = 0.0;

// Internal functions
void check_next_train();
double get_random_speed();
double get_random_length();
int get_random_interval();
struct timespec timediff(struct timespec* a, struct timespec* b);
double todbl(struct timespec ts);

double get_front_location()
{
    check_next_train();
    
    if (paused_front > 0.01) return paused_front;
    
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    
    return todbl(timediff(&now, &appear_time)) * speed;
}

double get_back_location()
{
    check_next_train();
    
    if (paused_front > 0.01) return paused_front - length;
    
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    
    return todbl(timediff(&now, &appear_time)) * speed - length;
}

void pause_train()
{
    printf("Paused train\n");
    clock_gettime(CLOCK_MONOTONIC, &pause_time);
    paused_front = get_front_location();
}

void start_train()
{
    printf("Continued train\n");
    
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    
    appear_time.tv_sec += timediff(&now, &pause_time).tv_sec;
    appear_time.tv_nsec += timediff(&now, &pause_time).tv_nsec;
    paused_front = 0.0;
}

void check_next_train()
{
    // We can never generate a new train if paused
    if (paused_front > 0.01) return;
    
    // Check if the train has left the track section
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    
    double tail = todbl(timediff(&now, &appear_time)) * speed - length;
    
    // If end of train has left
    if (tail > DIST_TOTAL) {
        struct timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
        
        // Some time may have passed since the train has left, but this is not
        // an important detail
        appear_time.tv_sec = now.tv_sec + get_random_interval();
        appear_time.tv_nsec = now.tv_nsec;
        
        speed = get_random_speed();
        length = get_random_length();
        
        printf("[%f] New train:\nat=%f (%f)\t speed=%f\t length=%f\n",
                todbl(now), todbl(appear_time),
                todbl(timediff(&appear_time, &now)),
                speed, length);
    }
}

double get_random_speed()
{
    // TRAIN_MIN_SPEED <= speed [m/s] < TRAIN_MAX_SPEED
    return (rand() % (TRAIN_MAX_SPEED-TRAIN_MIN_SPEED)) + TRAIN_MIN_SPEED;
}

double get_random_length()
{
    // TRAIN_MIN_LENGTH <= length/10 < TRAIN_MAX_LENGTH
    return (rand() % (TRAIN_MAX_LENGTH-TRAIN_MIN_LENGTH)) * 10.0;
}

int get_random_interval()
{
    // 0 <= interval [s] < TRAIN_MAX_INTERVAL
    return rand() % TRAIN_MAX_INTERVAL;
}

struct timespec timediff(struct timespec* a, struct timespec* b) {
    int sec = a->tv_sec - b->tv_sec;
    int ns = a->tv_nsec - b->tv_nsec; // To ms
    
    struct timespec ret;
    ret.tv_sec = sec;
    ret.tv_nsec = ns;
    return ret;
}

double todbl(struct timespec ts){
    double ret = ts.tv_sec;
    ret += ts.tv_nsec/1e9;
    return ret;
}

void print_train()
{
    double scaling = DIST_TOTAL/100;
    
    double front = get_front_location()/scaling;
    double back = get_back_location()/scaling;
    
    int i;
    // Upper track
    for (i=0; i<100; i++) {
        printf("-");
    }
    printf("\n");
    
    // Train
    if (back >= 0.0) {
        for (i=0; i<(int) back; ++i) {
            printf(" ");
        }
        printf("[");
        for (i=back+1; i<(int) front; ++i) {
            printf("#");
        }
        printf("]");
        printf("\n");
    } else {
        for (i=0; i<(int) front; ++i) {
            printf("#");
        }
        if (front > 0.01) printf("]");
        printf("\n");
    }
    
    // Lower track
    for (i=0; i<100; i++) {
        printf("-");
    }
    printf("\n");
    for (i=0; i<(int) (DIST_A/scaling); ++i) {
        printf(" ");
    }
    printf("^");
    for (i=(int)(DIST_A/scaling)+1; i<(int) ((DIST_A+DIST_B)/scaling); ++i) {
        printf(" ");
    }
    printf("^");
    for (i=(int)((DIST_A+DIST_B)/scaling)+2; i<(int) (DIST_TOTAL/scaling); ++i) {
        printf(" ");
    }
    printf("^\n\n\n");
}
