#ifndef SENSOR_H_
#define SENSOR_H_

/*
 The sensors are positioned as followed:
                     |     |
> -|--|--|--|--|--|--|     |--|--|-
> -|--|--|--|--|--|--|     |--|--|-
                     |     |
   ^           ^              ^
   sensor_a    sensor_b       sensor_c
 
   |   DIST_B  |     DIST_C   |
 */

#define DIST_A 0.0 // meter, is at the start
#define DIST_B 250.0 // meter
#define DIST_C 100.0 // meter
#define DIST_TOTAL (DIST_A+DIST_B+DIST_C)

enum sensor_state {
    no_train = 0,
    train
};

enum sensor_location {
    sensor_a = 0,
    sensor_b,
    sensor_c
};

#endif /* SENSOR_H_ */
